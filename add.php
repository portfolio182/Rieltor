<?php
require_once __DIR__.'/boot.php';

$user = null;

if (check_auth()) {
    // Получим данные пользователя по сохранённому идентификатору
    $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `id` = :id");
    $stmt->execute(['id' => $_SESSION['user_id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Риелтор</title>
  <link rel="stylesheet" href="css/bootstrap.css">
</head>

    <style>
html,
body {
  height: 100%;
}

registration {
  display: flex;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}


.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}

.form-signin .checkbox {
  font-weight: 400;
}

.form-signin .form-floating:focus-within {
  z-index: 2;
}

.form-signin input[type="text"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}

.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.form-edit {
  width: 100%;
  max-width: 660px;
  padding: 15px;
  margin: auto;
}

.form-edit .checkbox {
  font-weight: 400;
}

.form-edit {
  z-index: 2;
}

.form-edit input[type="text"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}

.form-edit input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.feather {
  width: 16px;
  height: 16px;
  vertical-align: text-bottom;
}

/*
 * Sidebar
 */

.sidebar {
  position: fixed;
  top: 0;
  /* rtl:raw:
  right: 0;
  */
  bottom: 0;
  /* rtl:remove */
  left: 0;
  z-index: 100; /* Behind the navbar */
  padding: 48px 0 0; /* Height of navbar */
  box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
}

@media (max-width: 767.98px) {
  .sidebar {
    top: 5rem;
  }
}

.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: .5rem;
  overflow-x: hidden;
  overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
}

.sidebar .nav-link {
  font-weight: 500;
  color: #333;
}

.sidebar .nav-link .feather {
  margin-right: 4px;
  color: #727272;
}

.sidebar .nav-link.active {
  color: #2470dc;
}

.sidebar .nav-link:hover .feather,
.sidebar .nav-link.active .feather {
  color: inherit;
}

.sidebar-heading {
  font-size: .75rem;
  text-transform: uppercase;
}

/*
 * Navbar
 */

.navbar-brand {
  padding-top: .75rem;
  padding-bottom: .75rem;
  font-size: 1rem;
  background-color: rgba(0, 0, 0, .25);
  box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
}

.navbar .navbar-toggler {
  top: .25rem;
  right: 1rem;
}

.navbar .form-control {
  padding: .75rem 1rem;
  border-width: 0;
  border-radius: 0;
}

.form-control-dark {
  color: #fff;
  background-color: rgba(255, 255, 255, .1);
  border-color: rgba(255, 255, 255, .1);
}

.form-control-dark:focus {
  border-color: transparent;
  box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);
}
    </style>

    

<body>
	
	<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap shadow p-3 text-white d-flex flex-wrap align-items-center justify-content-center">
		<a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 " href="#">Риелтор</a>

		<form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
			<input type="search" class="form-control form-control-dark" placeholder="Поиск..." aria-label="Поиск">
		</form>
		
		<?php if ($user) { ?>


		<div class="navbar-nav">
			
			<div class="nav-item text-nowrap">
				          

				<form method="post" action="do_logout.php">
					<button type="submit" class="btn btn-outline-light">Выйти</button>
					<a class="btn btn-warning"><?=htmlspecialchars($user['username'])?></a>
				</form>
			</div>
		</div>
		
		<?php } else { ?>
		<div class="navbar-nav">
			<div class="nav-item text-nowrap">
				<a class="btn btn-outline-light" href="login.php">Вход</a>
				<a type="button" class="btn btn-warning" href="register.php">Зарегистрироваться</a>
			</div>
		</div>
		<?php } ?>
	</header>

    <?php if ($user) { ?>
	<div class="text-center" id="registration">
  <main class="form-edit">
    <div class="py-5 text-center">
      <h2>Добавить новый</h2>
      <p class="lead">Для публикации информации об объекте внесите все требуемые данные.</p>
    </div>

    <div class="row g-5">
      <div ">
        <h4 class="mb-3">Объект</h4>
        <form method="post" enctype="multipart/form-data" class="needs-validation" action="do_add.php" novalidate>
			
			<!--  -->
			
          <div class="row g-3">
			  
            <div class="col-12">
              <label for="name" class="form-label">Название объекта</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Самый лучший дом для вашей семьи" required>
              <div class="invalid-feedback">
                Пожалуйста, введите название объекта.
              </div>
            </div>
            
            <div class="col-sm-6">
              <label for="address_street" class="form-label">Улица</label>
              <div class="input-group has-validation">
                <span class="input-group-text">улица</span>
                <input type="text" class="form-control" id="address_street" name="address_street" placeholder="Фокина" required>
              <div class="invalid-feedback">
                  Улица обязательно.
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <label for="address_number" class="form-label">Номер дома</label>
              <div class="input-group has-validation">
                <span class="input-group-text">дом</span>
                <input type="text" class="form-control" id="address_number" name="address_number" placeholder="12" required>
              <div class="invalid-feedback">
                  Номер дома обязательно.
                </div>
              </div>
            </div>

            <div class="col-12">
              <label for="address_number" class="form-label">Цена</label>
              <div class="input-group has-validation">
                
                <input type="text" class="form-control" id="price" name="price" placeholder="12" required>
                <span class="input-group-text">$</span>
              <div class="invalid-feedback">
                  Цена обязательно.
                </div>
              </div>
            </div>

            <div class="col-12">
              <label for="description" class="form-label">Описание <span class="text-muted">(Необязательно)</span></label>
              <input type="text" class="form-control" id="description" name="description" placeholder="до 1000 символов">
            </div>

          <hr class="my-4">

          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="status" name="status">
            <label class="form-check-label" for="status">Информация актуальна</label>
          </div>

          <hr class="my-4">
          
		<div class="mb-3">
			<label for="formFile" class="form-label">Изображение</label>
			<input class="form-control" type="file" id="formFile" name="filename">
		</div>
			
          <button class="w-100 btn btn-primary btn-lg" type="submit">Добавить объект</button>
        </form>

      </div>
    </div>
  </main>
    </div>
    
	<?php } else { ?>
	<?php } ?>

      <script src="form-validation.js" crossorigin="anonymous"></script>
</body>
</html>
