<?php

require_once __DIR__.'/boot.php';

if (check_auth()) {
    header('Location: /');
    die;
}
?>
<!doctype html>

<html lang="en">
	
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Вход</title>
	<link rel="stylesheet" href="css/bootstrap.css">
</head>

    <style>
html,
body {
  height: 100%;
}

registration {
  display: flex;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}

.form-signin .checkbox {
  font-weight: 400;
}

.form-signin .form-floating:focus-within {
  z-index: 2;
}

.form-signin input[type="text"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}

.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.feather {
  width: 16px;
  height: 16px;
  vertical-align: text-bottom;
}

/*
 * Sidebar
 */

.sidebar {
  position: fixed;
  top: 0;
  /* rtl:raw:
  right: 0;
  */
  bottom: 0;
  /* rtl:remove */
  left: 0;
  z-index: 100; /* Behind the navbar */
  padding: 48px 0 0; /* Height of navbar */
  box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
}

@media (max-width: 767.98px) {
  .sidebar {
    top: 5rem;
  }
}

.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: .5rem;
  overflow-x: hidden;
  overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
}

.sidebar .nav-link {
  font-weight: 500;
  color: #333;
}

.sidebar .nav-link .feather {
  margin-right: 4px;
  color: #727272;
}

.sidebar .nav-link.active {
  color: #2470dc;
}

.sidebar .nav-link:hover .feather,
.sidebar .nav-link.active .feather {
  color: inherit;
}

.sidebar-heading {
  font-size: .75rem;
  text-transform: uppercase;
}

/*
 * Navbar
 */

.navbar-brand {
  padding-top: .75rem;
  padding-bottom: .75rem;
  font-size: 1rem;
  background-color: rgba(0, 0, 0, .25);
  box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
}

.navbar .navbar-toggler {
  top: .25rem;
  right: 1rem;
}

.navbar .form-control {
  padding: .75rem 1rem;
  border-width: 0;
  border-radius: 0;
}

.form-control-dark {
  color: #fff;
  background-color: rgba(255, 255, 255, .1);
  border-color: rgba(255, 255, 255, .1);
}

.form-control-dark:focus {
  border-color: transparent;
  box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);
}
    </style>


<body>
	<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap shadow p-3 text-white d-flex flex-wrap align-items-center justify-content-center">
		<a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 " href="/">Риелтор</a>

		<form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
			<input type="search" class="form-control form-control-dark" placeholder="Поиск..." aria-label="Поиск">
		</form>
		
		<?php if ($user) { ?>
		<div class="navbar-nav">
			<div class="nav-item text-nowrap">
				<form method="post" action="do_logout.php">
					<button type="submit" class="btn btn-outline-light">Выйти</button>
				</form>
			</div>
		</div>
		
		<?php } else { ?>
		<div class="navbar-nav">
			<div class="nav-item text-nowrap">
				<a class="btn btn-outline-light" href="login.php">Вход</a>
				<a type="button" class="btn btn-warning" href="register.php">Зарегистрироваться</a>
			</div>
		</div>
		<?php } ?>
	</header>

	<div class="text-center">
		<main class="form-signin">
				<h1 class="h3 mb-3 fw-normal">Вход</h1>
				<?php flash() ?>

				<form method="post" action="do_login.php">
					<div class="form-floating">
						<input type="text" class="form-control" id="username" name="username" placeholder="Имя" required>
						<label for="username" class="form-label">Имя</label>
					</div>
					
					<div class="form-floating">
						<input type="password" class="form-control" id="password" name="password" placeholder="Пароль" required>
						<label for="password" class="form-label">Пароль</label>
					</div>
					
					<div class="d-flex justify-content-between">
						<button type="submit" class="btn btn-primary">Войти</button>
					</div>
				</form>
				            <p class="mt-5 mb-3 text-muted">© 2022</p>
				</main>
	</div>

</body>
</html>
